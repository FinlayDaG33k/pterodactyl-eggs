#!/usr/bin/env bash
registry="registry.gitlab.com"
repo="/finlaydag33k/pterodactyl-eggs"

# Check if Docker is installed
docker_loc=$(command -v docker)
if ! [ -x "${docker_loc}" ]; then
  echo "[ERROR] Docker not found, please install it!"
  exit 1
fi

# Check if the specified egg has a dockerfile
if [ ! -f "./$1/dockerfile" ]; then
  echo "[ERR] Dockerfile for \"$1\" does not exist!"
  exit 1
fi

docker build -t ${registry}${repo}:$1 -f ./$1/dockerfile ./$1