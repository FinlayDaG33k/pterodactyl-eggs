#!/usr/bin/env bash
registry="registry.gitlab.com"
repo="/finlaydag33k/pterodactyl-eggs"

# Check if Docker is installed
docker_loc=$(command -v docker)
if ! [ -x "${docker_loc}" ]; then
  echo "[ERROR] Docker not found, please install it!"
  exit 1
fi

# Check if the specified egg has been built
if [[ "$(docker images -q ${registry}${repo}:$1 2> /dev/null)" == "" ]]; then
  echo "[ERROR] Image for $1 has not been built yet!";
  exit
fi

docker login ${registry}
docker push ${registry}${repo}:$1