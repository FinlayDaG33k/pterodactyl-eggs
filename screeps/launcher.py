## PLEASE DO NOT MODIFY THIS FILE IF YOU DON'T KNOW WHAT YOU'RE DOING HERE
import subprocess
import sys

while True:
  # Ask the user what they want to do
  print "Please select an option:"
  print "0] Start Server"
  print "1] Install Mod (or any other NPM package)"
  choice = int(raw_input())

  # Check the choice that has been made
  if choice == 0:
    # User wants to start the server, exit this script
    exit(0)
  elif choice == 1:
    # User wants to install a mod
    # Ask the package name
    print "Please specify the package name as found on NPM: "
    package = str(raw_input())

    # Install the package
    print ""
    try:
      subprocess.check_output(['yarn', 'add', 'screepsmod-auth'])
      print "Installation complete"
    except:
      print "oh dear, something went wrong..."
      print "Please look at the error above to find out what!"
    print ""