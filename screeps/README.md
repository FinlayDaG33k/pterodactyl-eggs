# Screeps
Screeps is a MMO RTS sandbox game for programmers, wherein the core mechanic is programming your units AI.  
You control your colony by writing JavaScript which operate 24\/7 in the single persistent world filled by other players on par with you.

## Server Ports
The screeps server requires a single port for access (default 21025) but port can be assigned as well (default 21026).


| Port  | default |
|-------|---------|
| Game  | 21025   |
| CLI   | 21026   |